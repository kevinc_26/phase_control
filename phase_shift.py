import numpy as np
import matplotlib.pyplot as plt


def checkerboard(arr):
    return np.indices(arr.shape).sum(axis=0) % 2


class PhaseShifter(object):
    def __init__(self, input_im=None):
        self.im = plt.imread(input_im)

    def show(self):
        plt.imshow(self.im)
        plt.show()

    def apply_phase_shift(self, method, rgb_channel = 0):
        if method == 'checkerboard':
            self.im[:, :, rgb_channel] *= checkerboard(self.im[:,:,0])

if __name__ == '__main__':
    image = 'rotated_3x3.png'
    phase_im = PhaseShifter(image)
    phase_im.apply_phase_shift('checkerboard')
    phase_im.show()